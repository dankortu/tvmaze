import React from 'react';
import { Link } from 'react-router-dom';
import ReactHtmlParser from 'react-html-parser';

const ShowPreview = props => {
  const { id, image, name, summary } = props;
  return (
    <div className="films_copy">
      {image && <img src={image} />}
      <Link to={`/show/${id}`}>{name}</Link>
      <div className="summary">{ReactHtmlParser(summary)}</div>
    </div>
  );
};

export default ShowPreview;
