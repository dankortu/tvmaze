import './AppRouter.css';
import React, { Component } from 'react';
import Search from 'components/Search';
import ShowPage from 'components/ShowPage';
import { Switch, Route, withRouter, Redirect } from 'react-router-dom';

export class AppRouter extends Component {
  render() {
    return (
      <div className="App">
        <Switch>
          <Route path="/" component={Search} exact />
          <Route path="/show/:id" component={ShowPage} />
          <Redirect to="/" />
        </Switch>
      </div>
    );
  }
}

export default withRouter(AppRouter);
