import React, { Component } from 'react';
import { connect } from 'react-redux';
import { showRequest } from '../../actions/show';
import { getFilmInstance } from '../../reducers/shows';

class ShowPage extends Component {
  componentDidMount() {
    const { showRequest, match } = this.props;
    showRequest(match.params.id);
  }

  render() {
    const { filmInstance } = this.props;

    if (Object.keys(filmInstance).length) {
      return (
        <div className="film-instance">
          {filmInstance.image ? (
            <img src={filmInstance.image.medium} />
          ) : (
            <div className="img-not-found">НЕТ ИЗОБРАЖЕНИЯ</div>
          )}

          <p>{filmInstance.name}</p>

          <div className="cast">
            {filmInstance._embedded.cast.map((item, index) => (
              <div className="person" key={index}>
                {item.person.image ? (
                  <img src={item.person.image.medium} />
                ) : (
                  <div className="img-not-found">НЕТ ИЗОБРАЖЕНИЯ</div>
                )}
                <p>{item.person.name}</p>
              </div>
            ))}
          </div>
        </div>
      );
    } else {
      return <div>Идет загрузка</div>;
    }
  }
}

const mapStateToProps = state => ({
  filmInstance: getFilmInstance(state)
});

const mapDispatchToProps = { showRequest };

export default connect(mapStateToProps, mapDispatchToProps)(ShowPage);
