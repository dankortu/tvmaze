import React, { Component } from 'react';
import { connect } from 'react-redux';
import { searchRequest } from '../../actions/search';
import { getError, getFilms, getIsLoading } from '../../reducers/search';
import ShowPreview from '../ShowPreview/ShowPreview';

class Search extends Component {
  handleInputChange = event => {
    this.props.searchRequest(event.target.value);
  };

  render() {
    const {
      props: { isLoading, films },
      handleInputChange
    } = this;

    return (
      <div className="App">
        <h1>Поисковичок</h1>
        <input
          name="search"
          type="text"
          placeholder="Battlestar Galactica"
          onChange={handleInputChange}
        />

        {isLoading && <div className="loading">Идет загрузка</div>}

        {!isLoading && !films.length && (
          <div className="result">По данному запросу нет результата</div>
        )}

        <div className="films">
          {films.map(film => (
            <ShowPreview key={film.id} {...film} />
          ))}
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  films: getFilms(state),
  isLoading: getIsLoading(state),
  error: getError(state)
});

const mapDispatchToProps = { searchRequest };

export default connect(mapStateToProps, mapDispatchToProps)(Search);
