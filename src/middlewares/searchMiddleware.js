import { searchFailure, searchSuccess, searchRequest } from '../actions/search';
import { search } from '../api';

const searchMiddleware = store => next => action => {
  if (action.type === searchRequest.toString()) {
    search(action.payload)
      .then(shows => {
        const films = shows.map(show => ({
          name: show.name,
          image: show.image.medium,
          summary: show.summary,
          id: show.id
        }));
        store.dispatch(searchSuccess(films));
      })
      .catch(error => {
        store.dispatch(searchFailure(error));
      });
  }
  return next(action);
};

export default searchMiddleware;
