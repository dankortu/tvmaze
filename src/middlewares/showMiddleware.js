import { showSuccess, showRequest } from '../actions/show';
import { show } from '../api';

const showMiddleware = store => next => action => {
  if (action.type === showRequest.toString()) {
    show(action.payload).then(show => {
      store.dispatch(showSuccess(show));
    });
  }
  return next(action);
};

export default showMiddleware;
