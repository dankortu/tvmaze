import { showSuccess } from '../actions/show';
import { handleActions } from 'redux-actions';

const filmInstance = handleActions(
  {
    [showSuccess]: (state, action) => action.payload || state.filmInstance
  },
  {}
);

export default filmInstance;

export const getFilmInstance = state => state.shows;
