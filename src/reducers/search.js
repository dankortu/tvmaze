import { searchRequest, searchSuccess, searchFailure } from '../actions/search';
import { handleActions } from 'redux-actions';
import { combineReducers } from 'redux';

const isLoading = handleActions(
  {
    [searchRequest]: () => true,
    [searchSuccess]: () => false,
    [searchFailure]: () => false
  },
  false
);

const films = handleActions(
  {
    [searchSuccess]: (state, action) => action.payload
  },
  []
);

const error = handleActions(
  {
    [searchRequest]: () => null,
    [searchSuccess]: () => null,
    [searchFailure]: (state, action) => action.payload
  },
  null
);

export default combineReducers({
  isLoading,
  films,
  error
});

export const getIsLoading = state => state.search.isLoading;
export const getFilms = state => state.search.films;
export const getError = state => state.search.error;
